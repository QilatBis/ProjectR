package fr.projectr.api.network.packet;

import com.google.common.base.Joiner;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by Theophile on 17/11/2018 for ProjectR.
 */
public class PacketHandler {

    private static final String PACKET_SEPARATOR = "#SEPARATORPACKET543525421#";
    private static PacketHandler instance;
    @Getter
    private HashMap<String, Class<? extends Packet>> packetMap = new HashMap<>();
    @Getter
    private HashMap<Class<? extends Packet>, String> reversePacketMap = new HashMap<>();

    public PacketHandler() {
        instance = this;
    }

    public static PacketHandler get() {
        return instance;
    }

    public void registerPacket(String packetTag, Class<? extends Packet> clazz) {
        this.packetMap.put(packetTag, clazz);
        this.reversePacketMap.put(clazz, packetTag);
    }

    public void receivedPacket(String string) {
        String[] splitted = string.split(PACKET_SEPARATOR);
        String packetTag = splitted[0];
        Class<? extends Packet> packetClass = packetMap.get(packetTag);

        try {
            Packet packet = packetClass.newInstance();
            splitted = Arrays.copyOfRange(splitted, 1, splitted.length);
            packet.read(splitted);
            packet.act();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public String getStringtifiedPacket(Packet packet) {
        String[] writedPacket = packet.write();
        String[] packetComponent = new String[writedPacket.length + 1];

        packetComponent[0] = reversePacketMap.get(packet.getClass());
        System.arraycopy(writedPacket, 0, packetComponent, 1, writedPacket.length);

        return Joiner.on(PACKET_SEPARATOR).join(packetComponent);
    }

}
