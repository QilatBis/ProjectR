package fr.projectr.api;

import fr.projectr.api.utils.config.Configuration;
import org.apache.log4j.Logger;

import java.io.File;

/**
 * Created by Qilat on 30/09/2017 for ProjectR.
 */
public class API {

    private static Logger logger;
    private static String defaultPath;
    private static Configuration config;

    public API(Logger logger, String defaultPath){
        API.logger = logger;
        API.defaultPath = defaultPath;

        //Lang.reload();
    }
    public API(Logger logger, File defaultPath){
        this(logger, defaultPath.getAbsolutePath());
    }

    public static Logger getLogger() {
        return logger;
    }
    public static String getDefaultPath() {
        return defaultPath;
    }

    public static Configuration getConfig() {
        return config;
    }

}
