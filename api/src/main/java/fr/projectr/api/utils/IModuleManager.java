package fr.projectr.api.utils;

public interface IModuleManager {

    void registerModule(IModule module);

    void registerModules();

}
