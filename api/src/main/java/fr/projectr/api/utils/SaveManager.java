package fr.projectr.api.utils;

import lombok.Getter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Qilat on 29/09/2017 for ProjectR.
 */
public class SaveManager implements IModule{

    private static SaveManager manager;
    private static String OS = System.getProperty("os.name").toLowerCase();

    public static SaveManager get(){
        return manager;
    }

    @Getter
    private File parentFolder;
    private File saveFile;
    @Getter
    private File saveDBFile;

    private String suffix = "/ProjectR/";

    public static boolean isWindows() {

        return (OS.contains("win"));

    }

    public void createSaveFile(){
        saveFile = new File(parentFolder.getAbsolutePath() + "/save.txt");
        if(!saveFile.exists()){
            try {
                saveFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(saveFile));
            writer.write("hello");

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean init() {
        SaveManager.manager = this;
        try {
            SaveManager.get().configure();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void registerCommands() {

    }

    @Override
    public void stop() {

    }

    public static boolean isMac() {

        return (OS.contains("mac"));

    }

    public static boolean isUnix() {

        return (OS.contains("nix") || OS.contains("nux") || OS.indexOf("aix") > 0);

    }

    public void configure() throws IOException {

        if (isWindows()) {
            parentFolder = new File(System.getenv("APPDATA") + suffix);
        } else if (isMac()) {
            parentFolder = new File(System.getProperty("user.home") + "/Library/Application Support" + suffix);

        } else if (isUnix()) {
            parentFolder = new File(System.getProperty("user.home") + suffix);
        }

        this.saveDBFile = new File(parentFolder, "saveDb.db");
        if (this.saveDBFile.exists()) {
            this.saveDBFile.createNewFile();
        }
    }
}

