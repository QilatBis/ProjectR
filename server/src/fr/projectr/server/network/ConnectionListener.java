package fr.projectr.server.network;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import fr.projectr.api.API;
import fr.projectr.api.network.ClientInfo;

public class ConnectionListener extends Listener {

    @Override
    public void connected(Connection connection) {
        API.getLogger().info(connection.getRemoteAddressTCP() + " just connect to this server.");
        NetworkServerManager.get().getClientInfos().put(connection.getID(), new ClientInfo(connection.getID(), connection));
    }
}
