package fr.projectr.server;

import com.badlogic.gdx.Gdx;
import fr.projectr.api.API;
import fr.projectr.api.utils.IModule;
import fr.projectr.api.utils.IModuleManager;
import fr.projectr.server.network.NetworkServerManager;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import java.io.File;
import java.util.ArrayList;

public class ProjectRServer implements IModuleManager {

    @Getter
    private static Logger logger = Logger.getLogger(ProjectRServer.class);
    @Getter
    private static API api;
    @Setter(AccessLevel.PRIVATE)
    @Getter
    private static Scheduler scheduler;
    @Getter
    private boolean local;
    @Getter
    private ArrayList<IModule> modules = new ArrayList<>();


    public ProjectRServer(boolean local) {
        this.local = local;
    }

    @Override
    public void registerModule(IModule module) {
        this.modules.add(module);
    }

    public static void main(String[] args) {
        new ProjectRServer(false).start();
    }

    @Override
    public void registerModules() {
        this.registerModule(new NetworkServerManager());
    }

    private void start() {
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.INFO);

        ProjectRServer.logger.info("Loading of API");
        api = new API(ProjectRServer.getLogger(), new File("./"));

        ProjectRServer.logger.info("Loading of Quartz Scheduler");
        try {
            ProjectRServer.setScheduler(StdSchedulerFactory.getDefaultScheduler());
            ProjectRServer.getScheduler().start();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

        ProjectRServer.getLogger().info("Loading Module");
        this.registerModules();

        for (IModule module : modules) {
            ProjectRServer.getLogger().info("[Module] " + module.getClass().getSimpleName() + " loading...");
            if (!module.init()) {
                ProjectRServer.getLogger().error("[Module] Cannot load " + module.getClass().getSimpleName() + " module. Shutdown...");
                Gdx.app.exit();
            }
        }
        for (IModule module : modules) {
            ProjectRServer.getLogger().info("[Module] " + module.getClass().getSimpleName() + " registering commands...");
            module.registerCommands();
        }

        ProjectRServer.getLogger().info("ProjectR ready");
    }
}
