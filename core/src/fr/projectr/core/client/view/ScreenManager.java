package fr.projectr.core.client.view;

import fr.projectr.core.client.ProjectRClient;
import fr.projectr.core.client.model.entity.EntityManager;
import fr.projectr.core.client.view.game.GameScreen;
import fr.projectr.core.client.view.game.map.HomeMap;
import fr.projectr.core.client.view.menu.mainmenu.MainMenuScreen;
import fr.projectr.core.client.view.ui.GameUI;

/**
 * Created by Theophile on 17/11/2018 for ProjectR.
 */
public class ScreenManager {

    private static ScreenManager manager;

    public ScreenManager() {
        manager = this;
    }

    public static ScreenManager get() {
        if (manager == null)
            new ScreenManager();
        return manager;
    }

    public void switchToScreen(ScreenType type) {
        switch (type) {
            case GAME:
                ProjectRClient.getCurrent().setScreen(new GameScreen(new HomeMap(EntityManager.get().getMainPlayer()), new GameUI()));
                break;
            case MAINMENU:
                ProjectRClient.getCurrent().setScreen(new MainMenuScreen());
                break;
        }
    }

    public enum ScreenType {
        GAME,
        MAINMENU,
        OPTIONS
    }

}
