package fr.projectr.core.client.view.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import fr.projectr.core.client.view.game.map.Map;
import fr.projectr.core.client.view.ui.GameUI;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

public class GameScreen implements Screen {
    public static final float STANDARD_SCALE = 1 / 64f;
    @Getter
    private Map map;
    @Getter
    private GameUI gameUI;
    @Getter
    private OrthogonalTiledMapRenderer renderer;
    @Setter(AccessLevel.PRIVATE)
    @Getter
    private Camera camera;

    public GameScreen(Map map, GameUI gameUI) {
        this.map = map;
        this.setCamera(this.map.getStage().getCamera());
        this.getCamera().position.x = 0;
        this.getCamera().position.y = 0;

        this.renderer = new OrthogonalTiledMapRenderer(this.map.getTiledMap(), GameScreen.STANDARD_SCALE);

        this.gameUI = gameUI;

        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(this.getMap().getStage());
        inputMultiplexer.addProcessor(this.getGameUI().getStage());
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        this.updateCameraPosition();

        this.getRenderer().setView((OrthographicCamera) this.getCamera());
        this.getRenderer().render();

        this.getMap().getPlayer().handleInputMovement();

        this.getMap().getStage().act();
        this.getGameUI().getStage().act();

        this.getMap().getStage().draw();
        this.getGameUI().getStage().draw();

        this.getMap().doPhysicsStep(delta);
        // this.getMap().getDebugRenderer().render(this.getMap().getWorld(), this.getCamera().combined);
    }

    private void updateCameraPosition() {

        this.camera.position.x = MathUtils.clamp(this.map.getPlayer().getBody().getPosition().x,
                (this.camera.viewportWidth / 2f),
                ((TiledMapTileLayer) this.getMap().getTiledMap().getLayers().get(0)).getWidth() - (this.camera.viewportWidth / 2f));

        this.camera.position.y = MathUtils.clamp(this.map.getPlayer().getBody().getPosition().y,
                (this.camera.viewportHeight / 2f),
                ((TiledMapTileLayer) this.getMap().getTiledMap().getLayers().get(0)).getHeight() - (this.camera.viewportHeight / 2f));

    }

    @Override
    public void resize(int width, int height) {
        this.getCamera().viewportWidth = 30f;
        this.getCamera().viewportHeight = 30f * height / width;
        this.getCamera().update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
