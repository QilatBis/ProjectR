package fr.projectr.core.client.view.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import fr.projectr.core.client.model.chat.ChatManager;
import fr.projectr.core.client.model.console.message.Message;
import fr.projectr.core.client.model.console.message.MessageType;
import fr.projectr.core.client.model.entity.EntityManager;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 04/09/2017.
 */
public class ChatUI extends WidgetGroup {

    @Getter
    private static ChatUI instance;

    @Getter
    private static TextField textField;
    private static TextArea textArea;
    private static ScrollPane pane;

    @Getter
    @Setter
    private static boolean textBarFocused = false;

    public ChatUI() {
        super();

        instance = this;

        this.setSize(400, 200);

        Skin skin = new Skin(Gdx.files.internal("texture/ui/uiskin.json"));

        textField = new TextField("", skin, "default");
        textField.setSize(this.getWidth(), 30);
        this.addActor(textField);

        TextField.TextFieldStyle style = skin.get("default", TextField.TextFieldStyle.class);
        textArea = new TextArea("", style);
        textArea.setBounds(textField.getX(), textField.getHeight(), this.getWidth(), this.getHeight() - textField.getHeight());
        textArea.setTouchable(Touchable.disabled);
        textArea.setPrefRows(ChatManager.getMsgList().size());


        pane = new ScrollPane(textArea, skin);
        pane.setForceScroll(false, true);
        pane.setFlickScroll(false);
        pane.setOverscroll(false, true);
        pane.setBounds(textArea.getX(), textArea.getY(), textArea.getWidth(), textArea.getHeight());
        pane.setScrollPercentY(1f);
        this.addActor(pane);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, 0.7f);
    }

    public static void sendMessage(String msg) {
        ChatManager.analyzeMsgAndAct(Message.builder(EntityManager.get().getMainPlayer()).type(MessageType.DEFAULT).append(msg).build());
        getTextField().setText("");
    }

    public static void updateChat() {
        textArea.setText(ChatManager.getRenderString());
        pane.layout();
        textArea.setPrefRows((ChatManager.getMsgList().size() + 1) * 0.8f);
        pane.setScrollPercentY(1f);
    }


}
