package fr.projectr.core.client.view.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by Qilat on 07/09/2017.
 */
public class UpUI extends WidgetGroup {

    private Texture background = new Texture("texture/ui/up_border.png");

    private Button teamButton;
    private Button menuButton;

    public UpUI() {

        Image background = new Image(this.background);


        //background.setSize(this.getWidth(), background.getHeight());
        background.setPosition(0, 0);
        this.addActor(background);

        Button invButton = new Button(new Skin(Gdx.files.internal("texture/ui/uiskin.json")));

        invButton.setPosition(100, 100);

        this.addActor(invButton);

        invButton.add(new TextField("Click on Me", new Skin(Gdx.files.internal("texture/ui/uiskin.json"))));

        invButton.setBounds(0, 0, 100, 100);
        InputListener hallListener = new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        };

        invButton.addListener(hallListener);
    }

    public class Listener extends ClickListener {

    }

}
