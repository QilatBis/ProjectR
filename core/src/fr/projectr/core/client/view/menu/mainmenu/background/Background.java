package fr.projectr.core.client.view.menu.mainmenu.background;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import lombok.Getter;

/**
 * Created by Theophile on 17/11/2018 for ProjectR.
 */
public class Background {

    @Getter
    private Stage stage;
    @Getter
    private Texture backGroundTexture = new Texture("texture/menu/background.png");
    @Getter
    private Image backgroundImage;

    public Background() {
        this.stage = new Stage(new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
        this.backgroundImage = new Image(new SpriteDrawable(new Sprite(backGroundTexture)));

        this.backgroundImage.setHeight(Gdx.graphics.getHeight());
        this.backgroundImage.setWidth(this.backgroundImage.getWidth() * Gdx.graphics.getWidth() / Gdx.graphics.getHeight());

        float x = this.getStage().getCamera().viewportWidth / 2 - this.backgroundImage.getWidth() / 2;
        float y = this.getStage().getCamera().viewportHeight / 2 - this.backgroundImage.getHeight() / 2;
        this.backgroundImage.setPosition(x, y);

        this.getStage().addActor(this.backgroundImage);

    }
}
