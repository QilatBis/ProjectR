package fr.projectr.core.client.model.console.message;

import fr.projectr.core.client.model.entity.CommandSender;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 13/09/2017.
 */
public class Message {
    @Setter(AccessLevel.PRIVATE)
    @Getter
    CommandSender sender;
    @Setter(AccessLevel.PRIVATE)
    @Getter
    String text;
    @Setter(AccessLevel.PRIVATE)
    @Getter
    MessageType type;

    private Message() {
        text = "";
    }

    public static Builder builder(CommandSender sender) {
        return new Builder(sender);
    }

    private void append(String str) {
        text += str;
    }

    @Override
    public String toString() {
        return text;
    }

    public String getCommandInput() {
        return this.getText().substring(1);
    }

    public boolean isCommand() {
        return text.charAt(0) == '/';
    }

    public static class Builder {
        @Setter(AccessLevel.PRIVATE)
        @Getter
        Message message;

        Builder(CommandSender sender) {
            message = new Message();
            message.setSender(sender);
        }

        public Builder append(String append) {
            this.getMessage().append(append);
            return this;
        }

        public Builder type(MessageType messageType) {
            this.getMessage().setType(messageType);
            return this;
        }

        public Message build() {
            return this.message;
        }
    }
}
