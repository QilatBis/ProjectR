package fr.projectr.core.client.model.entity;

import fr.projectr.api.utils.IModule;
import fr.projectr.core.client.model.entity.human.NPCHumanEntity;
import fr.projectr.core.client.model.entity.human.Player;
import lombok.Getter;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Theophile on 10/11/2018 for ProjectRClient.
 */
public class EntityManager implements IModule {

    private static EntityManager entityManager;

    public static EntityManager get() {
        return entityManager;
    }

    private HashMap<UUID, Entity> entities = new HashMap<>();

    @Getter
    private Player mainPlayer;

    @Override
    public boolean init() {
        entityManager = this;
        mainPlayer = new Player(getNewUUID());
        registerEntity(mainPlayer);
        return true;
    }

    @Override
    public void registerCommands() {

    }

    @Override
    public void stop() {

    }

    private void registerEntity(Entity entity) {
        this.entities.put(entity.getUuid(), entity);
    }

    public Entity getEntity(UUID uuid) {
        return entities.get(uuid);
    }

    private UUID getNewUUID() {
        UUID uuid;
        do {
            uuid = UUID.randomUUID();
        } while (entities.get(uuid) != null);
        return uuid;
    }

    public NPCHumanEntity createNPC(NPCDesc desc) {
        NPCHumanEntity entity = new NPCHumanEntity(getNewUUID(), desc);
        this.registerEntity(entity);
        return entity;
    }
}
