package fr.projectr.core.client.model.network;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Listener;
import fr.projectr.api.network.listener.ReceivedListener;
import fr.projectr.api.network.packet.Packet;
import fr.projectr.api.utils.IModule;
import lombok.Getter;

import java.io.IOException;

public class NetworkClientManager implements IModule {

    private static NetworkClientManager manager;
    @Getter
    private Client kryoClient;

    public static NetworkClientManager get() {
        return manager;
    }

    @Override
    public boolean init() {
        manager = this;
        try {
            this.kryoClient = new Client();
            this.kryoClient.start();
            this.kryoClient.connect(5000, "localhost", 54555, 54777);

            this.registerKryoListeners();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void registerCommands() {

    }

    @Override
    public void stop() {

    }

    public void registerKryoListener(Listener listener) {
        this.kryoClient.addListener(listener);
    }

    public void registerKryoListeners() {
        this.registerKryoListener(new ReceivedListener());
    }

    public void registerPackets() {

    }

    public void sendPacketToServer(Packet packet) {
        this.kryoClient.sendTCP(packet.toString());
    }

}
