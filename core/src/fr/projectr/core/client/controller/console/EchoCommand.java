package fr.projectr.core.client.controller.console;

import fr.projectr.core.client.model.command.Command;
import fr.projectr.core.client.model.entity.CommandSender;

/**
 * Created by Qilat on 14/08/2017.
 */
public class EchoCommand extends Command {

    public EchoCommand() {
        super("echo", "say");
    }

    @Override
    public boolean execute(CommandSender sender, Command cmd, String[] input) {
        if (input.length > 1) {
            for (int i = 1; i < input.length; i++)
                System.out.print(input[i] + " ");
            System.out.print("\n");
            return true;
        }
        return false;
    }
}
