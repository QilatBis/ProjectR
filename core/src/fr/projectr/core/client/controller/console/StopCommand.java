package fr.projectr.core.client.controller.console;

import com.badlogic.gdx.Gdx;
import fr.projectr.api.utils.enums.StopReason;
import fr.projectr.api.utils.lang.Lang;
import fr.projectr.core.client.ProjectRClient;
import fr.projectr.core.client.model.command.Command;
import fr.projectr.core.client.model.entity.CommandSender;

import java.util.HashMap;

/**
 * Created by Qilat on 14/08/2017.
 */
public class StopCommand extends Command {

    public StopCommand() {
        super("stop", "shutdown");
    }

    @Override
    public boolean execute(CommandSender sender, Command cmd, String[] args) {

        HashMap<String, String> metadata = new HashMap<>();
        metadata.put("reason", StopReason.CMD_CONSOLE.getString());
        ProjectRClient.getLogger().info(Lang.get("stop_game", metadata));

        Gdx.app.exit();

        return true;
    }
}
