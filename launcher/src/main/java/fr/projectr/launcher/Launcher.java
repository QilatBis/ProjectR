package fr.projectr.launcher;

import com.esotericsoftware.kryonet.Client;
import fr.projectr.api.API;
import fr.projectr.api.utils.SaveManager;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import lombok.Getter;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by Qilat on 19/09/2017 for ProjectR.
 */
public class Launcher extends Application{

    @Getter
    private Client client;
    @Getter
    private static API api;

    public static void main(String[] args){
        try {
            new SaveManager().configure();
        } catch (IOException e) {
            e.printStackTrace();
        }
        api = new API(Logger.getLogger(Launcher.class), SaveManager.get().getParentFolder());
        new KryoManager();
        new AuthManager();

        Launcher.launch(Launcher.class, args);
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        URL fxmlURL = getClass().getResource("../../../../resources/launcher.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);

        Pane root = fxmlLoader.load();

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);

        primaryStage.show();
    }

    public static void launchGame(String args[]){

        try {
            Process proc = Runtime.getRuntime().exec(new String[]{"java",  "-jar",  "/Users/Theophile/Library/Application Support/ProjectR/desktop-1.0.jar"});
            InputStream in = proc.getInputStream();
            InputStream err = proc.getErrorStream();

            System.out.println(convertStreamToString(in));
            System.out.println(convertStreamToString(err));

            proc.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }


}
